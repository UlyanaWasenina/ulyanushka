﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Практика_3._2
{
    class Program
    {
        static void Main(string[] args)
        {
            double p = 0;
            int k = 1;
            Console.Write("Введите длину массива, натуральное число n: n=");
            int n = int.Parse(Console.ReadLine());
            if (n > 0)
            {
                try
                {
                    int[] b = new int[n]; //создание массива длинны n
                    Console.WriteLine("Введите элементы массива");
                    for (int i = 0; i < n; i++)
                    {
                        Console.Write("b[{0}]=", i);
                        b[i] = int.Parse(Console.ReadLine()); //ввод элементов пользователем
                        p = Math.Pow(2, (k + 1));
                        k++;
                        Console.WriteLine("b[i]={1}, где i={0}", i, p);
                    }
                }
                catch (FormatException e) //обработка исключений 
                {
                    Console.WriteLine(e);
                }

            }
            else
            {
                Console.WriteLine("Ошибка! Внимательно читайте условие!!!");
                Console.Read();
            }
            Console.ReadLine();
        }
    }
}
