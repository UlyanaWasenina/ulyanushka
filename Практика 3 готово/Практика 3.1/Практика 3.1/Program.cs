﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Практика_3._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите натуральное число n, число элементов массива: n=");
            int n = int.Parse(Console.ReadLine());
            int p = 0; //присваивание нуля переменной, которая будет использоваться в цикле и далее в качестве ответа
            if (n>0) //условие на проверку выполнения условия задачи
            {
                try
                {
                    int[] a = new int[n]; //создание массива длинны n
                    Console.WriteLine("Введите элементы массива");
                    for (int i = 0; i < n; i++) 
                    {
                        Console.Write("a[{0}]=", i); 
                        a[i] = int.Parse(Console.ReadLine()); //ввод элементов пользователем
                        if ((i%2==0) && (a[i]%2==1))//если индекс четный и элемент нечетный
                        {
                            p++; //количество подходящих элементов увеличивается
                        }
                    }
                }
                catch (FormatException e) //обработка исключений 
                { 
                    Console.WriteLine(e);
                }
                Console.WriteLine("Количество элементов с нечетным индексом и являющихся четными числами, равно {0}", p);
                Console.ReadLine();
            }
            else //ошибка при несоблюдении условий
            {
                Console.WriteLine("Ошибка! Читайте внимательно условие для числа n!!!");
                Console.ReadLine();
            }
        }
    }
}

