﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Практика_6._1
{
    class Program
    {
        static void Main(string[] args)
        {
            int i, j, a;
            Console.WriteLine("Введите элементы массива");
            int[] f = new int[11]; //создание целочисленного массива
            for (i=0; i<=10; i++)
            {
                Console.Write("f[{0}]=", i);
                f[i] = Convert.ToInt32(Console.ReadLine()); //чтение элементов массива
            }
            a = f[0]; 
            if (a==0)
            {
                Console.Write("Ответ: x=0");
            }
            else
            {
                for (j=(-a); (j<0); j++) //поиск отрицательных делителей
                {
                    if (a%j==0) //если остаток от деления 0
                    {
                        Console.WriteLine("x={0}", j); //вывод делителя
                        double otvet = f[10] * Math.Pow(j, 10) + f[9] * Math.Pow(j, 9) + f[8] * Math.Pow(j, 8) + f[7] * Math.Pow(j, 7) + f[6] * Math.Pow(j, 6) + f[5] * Math.Pow(j, 5) + f[4] * Math.Pow(j, 4) + f[3] * Math.Pow(j, 3) + f[2] * Math.Pow(j, 2) + f[1] * j + f[0]; //вычисление формулы
                        if (otvet == 0) //если ответ формулы 0
                        {
                            Console.WriteLine("Ответ: x=" + j); //вывод подходящего ответа
                        }
                        else Console.WriteLine("Не ответ"); 
                    }
                }
                for (j=1; (j <= a); j++) //поиск положительных делителей
                {
                    if (a % j == 0) //если остаток от деления 0
                    {
                        Console.WriteLine("x={0}", j); //вывод делителя
                        double otvet = f[10] * Math.Pow(j, 10) + f[9] * Math.Pow(j, 9) + f[8] * Math.Pow(j, 8) + f[7] * Math.Pow(j, 7) + f[6] * Math.Pow(j, 6) + f[5] * Math.Pow(j, 5) + f[4] * Math.Pow(j, 4) + f[3] * Math.Pow(j, 3) + f[2] * Math.Pow(j, 2) + f[1] * j + f[0]; //вычисление формулы
                        if (otvet == 0) //если ответ формулы 0
                        {
                            Console.WriteLine("Ответ: x=" + j); //вывод подходящего ответа
                        }
                        else Console.WriteLine("Не ответ");
                    }
                }
            }
            Console.ReadLine();
        }
    }
}
