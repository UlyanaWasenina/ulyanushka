﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Практика_4._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите длину массива, натуральное число n: n=");
            int n = int.Parse(Console.ReadLine());
            if (n > 0)
            {
                int p = 0;
                int max = 0;
                int b, c;
                try
                {
                    int[] a = new int[n]; //создание массива длинны n
                    Console.WriteLine("Введите элементы массива");
                    for (int i = 0; i < n; i++)
                    {
                        Console.Write("a[{0}]=", i);
                        a[i] = int.Parse(Console.ReadLine()); //ввод элементов пользователем
                        if (a[i] % 2 == 0) //если элемент массива четный
                        {
                            p++; //+1 к количеству четных
                        }
                        else //если нечетные
                        {
                            if  (a[i] > max) //если следующий элемент больше
                            {
                                max = a[i]; //то он становится наибольшим
                            }
                        }

                    }

                    b = a[n - 1]; //элемент a[n]
                    c = b + 1; //элемент a[n]+1
                    Console.WriteLine("a[{0}]+1={1}", n, c); //вывод элемента a[n]+1 для наглядности
                    if (c % 2 == 0)
                    {
                        p++;
                    }
                    else
                    {
                        if (c > max)
                        {
                            max = c;
                        }
                    }



                }
                catch (FormatException e) //обработка исключений 
                {
                    Console.WriteLine(e);
                }
                Console.WriteLine("Количество четных чисел последовательности а(1),...,a(n), a(n)+1 равно {0}. Наибольшее из нечетных чисел последовательности равно {1}", p, max);
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("Ошибка! Внимательно читайте условие для n!");
                Console.ReadLine();
            }
        }
    }
}