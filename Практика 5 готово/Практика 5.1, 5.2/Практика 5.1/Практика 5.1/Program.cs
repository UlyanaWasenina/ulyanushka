﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Практика_5._1
{
    class Program
    {
        static void Main(string[] args)
        {
            int i, j;
            int p = 0;
            Console.Write("Введите натуральное число n, количество символов: n=");
            int n = Convert.ToInt32(Console.ReadLine()); //длина массива
            char[] a = new char[n];
            Random r = new Random(); //создание рандомного буквенного массива
            for  (i=0; i<n; i++)
            {
                a[i] = (char) r.Next(0x0410, 0x042F); //от а до я
                Console.Write(a[i] + " ");  //вывод массива на экран
            }
            for (i=0; i<n; i++) //просматриваем каждый элемент массива
            {
                if (a[i]==0x0414) //если элемент = Д
                {
                    Console.WriteLine("                  "); //оставляем немного места для наглядности
                    if (i>=15) //если порядковый номер элемента 15 или больше
                    {
                        for (j=(i-14); j<i; j++) 
                        {
                            Console.Write(a[j] + " "); //выписываем предыдущие 14 элементов
                        }
                    }
                   else //если порядковый номер буквы д меньше 15
                    {
                        for (j=0; j<i; j++) 
                        {
                            Console.Write(a[j] + " "); //выписываем все предыдущие элементы
                        }
                    }
                    Console.Write(a[i]); //дописываем в конце слова букву Д
                    p++; //считаем количество слов
                }
            }
            if (p==0) //если нет ни одного слова
            {
                Console.WriteLine(" нет таких слов");
            }
            Console.ReadKey(); //закрытие программы

        }
    }
}
