﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Практика_5._2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Количество строк ");
            int x = Convert.ToInt32(Console.ReadLine());
            Console.Write("Количество столбцов ");
            int y = Convert.ToInt32(Console.ReadLine());
            double[,] a = new double[x, y]; //создание массива х на у
            for (int i=0; i<x; i++) //цикл строк
            {
                for (int j=0; j<y;  j++) //цикл столбцов
                {
                    if ((i + j) != 0) //на 0 делить нельзя, для этого дополнительное словие
                    {
                        a[i, j] = Math.Pow((i + j), -1); //выполнение действия
                        a[i, j] = Math.Round(a[i, j], 2); //округление числа до сотых для удобства
                        Console.Write(string.Format("{0} ", a[i, j])); //вывод на экран
                    }
                    else
                    {
                        a[i, j] = 0; //зануляем элемент в целях исключения ошибки
                        Console.Write(string.Format("{0} ", a[i, j])); //вывод элемента
                    }
                }
                Console.Write(Environment.NewLine);
            }
            Console.ReadLine();
        }
    }
}
