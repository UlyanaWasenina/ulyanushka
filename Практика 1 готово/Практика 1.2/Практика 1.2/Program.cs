﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Практика_1._2
{
    class Program
    {
        static void Main(string[] args)
        {
            double f;
            Console.Write("Введите рациональное число: а=");
            double a = double.Parse(Console.ReadLine());
            if ((a>=-2) && (a<2))
            {
                f = Math.Pow(a, 2);
            }
            else
            {
                f = 4;
            } 
            Console.WriteLine("f(a)=f({0})={1}", a, f);
            Console.Write("Введите рациональное число: а=");
            double b = double.Parse(Console.ReadLine());
            if (b<=2)
            {
                f = b * b + 4 * b + 5;
            }
            else
            {
                f = 1 / (b * b + 4 * b + 5);
            }
            Console.WriteLine("f(a)=f({0})={1}", b, f);
            Console.ReadLine();

        }
    }
}
