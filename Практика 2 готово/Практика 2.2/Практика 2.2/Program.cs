﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Практика_2._2
{
    class Program
    {
        static void Main(string[] args)
        {
            double j; 
            int i = 1;
            Console.Write("Введите натуральное число n: n=");
            int n = int.Parse(Console.ReadLine());
            if  (n>0) //условие для проверки выполнения условия задачи
            {
                j = 1; 
                while (i <= n) //пока i меньше введенного пользователем числа, выполняются вычисления
                {
                    j = j * (1 + (1 / Math.Pow(i, 2))); //j-некоторый промежуточный результат, по окончании цикла станет ответом
                    i++; 
                }
                Console.WriteLine("Ответ: {0}", j);
                Console.ReadLine();
            }
            else //при невыполнении условия выдается ошибка, программа закрывается при нажатии на Enter
            {
                Console.WriteLine("Ошибка!!! Прочитайте внимательно условие для числа n!!!");
                Console.ReadLine();
            }

        }
    }
}
