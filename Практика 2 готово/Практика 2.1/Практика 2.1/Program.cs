﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Практика_2._1
{
    class Program
    {
        static void Main(string[] args)
        {
            int m;
            Console.Write("Введите натуральное число n, большее 99: n="); //сообщение для пользователя
            int n = int.Parse(Console.ReadLine()); //программа считывает число, указанное пользователем
            if (n > 99) 
            {
                m = n / 100; 
                if (m == 1)
                {
                    Console.Write("В числе {0} {1} сотня", n, m);
                    Console.ReadLine();
                };
                if ((m >= 2) & (m <= 5))
                {
                    Console.Write("В числе {0} {1} сотни", n, m);
                    Console.ReadLine();
                };
                if (m > 5)
                {
                    Console.Write("В числе {0} {1} сотен", n, m);
                    Console.ReadLine();

                }
            }
            else //если не выполняется условие и в числе нет сотен, то выдаётся ошибка и программа закрывается по нажатию на Enter
            {
                Console.WriteLine("Ошибка! Читайте условие для n!!!");
                Console.ReadLine();
            }
        }
    }
}